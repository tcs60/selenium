import org.junit.jupiter.api.Test;

/*
* An abstract class allows you to create functionality that subclasses can implement or override.
* An interface only allows you to define functionality, not implement it.
* And whereas a class can extend only one abstract class, it can take advantage of multiple interfaces.
*/

public class AbstrctInt {
    //Creating interface that has 4 methods
    interface A{
        void a();//bydefault it is public and abstract
        void b();
        void c();
        void d();
    }

    //Creating abstract class that provides the implementation of one method of A interface
    abstract class B implements A{
        public void c(){
            System.out.println("I am C");
        }
    }

    //Creating subclass of abstract class, now we need to provide the implementation of rest of the methods
    class M extends B{
        public void a(){
            System.out.println("I am a");
        }
        public void b(){
            System.out.println("I am b");
        }
        public void d(){
            System.out.println("I am d");
        }
    }
    @Test
    public void diffAbsInt(){
        A a=new M();
        a.a();
        a.b();
        a.c();
        a.d();
    }
}
