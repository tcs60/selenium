import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

/*
* Vector and ArrayList both uses Array internally as data structure. They are dynamically resizable.
* Difference is in the way they are internally resized.
* By default, Vector doubles the size of its array when its size is increased.
* But, ArrayList increases by half of its size when its size is increased.
* Vector is synchronized, which means only one thread at a time can access the code,
* while arrayList is not synchronized, which means multiple threads can work on arrayList at the same time.
*/

public class ArrayvVector {
    @Test
    public void aLvsVector(){
        // creating an ArrayList
        ArrayList<String> al = new ArrayList<String>();

        // adding object to arraylist
        al.add("Practice.GeeksforGeeks.org");
        al.add("quiz.GeeksforGeeks.org");
        al.add("code.GeeksforGeeks.org");
        al.add("contribute.GeeksforGeeks.org");

        // traversing elements using Iterator'
        System.out.println("ArrayList elements are:");
        Iterator it = al.iterator();
        while (it.hasNext())
            System.out.println(it.next());

        // creating Vector
        Vector<String> v = new Vector<String>();
        v.addElement("Practice");
        v.addElement("quiz");
        v.addElement("code");

        // traversing elements using Enumeration
        System.out.println("\nVector elements are:");
        Enumeration e = v.elements();
        while (e.hasMoreElements())
            System.out.println(e.nextElement());
    }
}
