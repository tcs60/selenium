import org.junit.jupiter.api.Test;

/*
* Garbage Collector can be defined as a program that is used to manage memory automatically
* by handling the object de-allocation.
* When new objects are created memory is allocated using the new operator.
* The memory allocated to an object using a new operator remains allocated until the references are using this object.
* As soon as the references cease to exist, the memory that the object occupies is reclaimed.
* Java then handles the de-allocation or destruction of objects automatically and we need not explicitly destroy the object.
* This technique is the Garbage Collection technique in Java where the programmers need not handle
* the deallocation of objects explicitly.
*/
public class GarbageCOL {
    class TestGC{
        @Override
        // finalize method: called on object once
        // before garbage collecting it
        protected void finalize() throws Throwable
        {
            System.out.println("Garbage collector called");
            System.out.println("Object garbage collected : " + this);
        }
    }
    @Test
    public void runGarbageCOL(){
        TestGC gc1=new TestGC();
        TestGC gc2=new TestGC();
        gc1 = null;  //nullify gc1

        System.gc();  //request for GC to run
        gc2 = null;  //nullify gc2
        Runtime.getRuntime().gc(); //request for GC to run
    }
}