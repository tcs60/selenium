import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.HashMap;

/*
* HashMap provides the basic implementation of the Map interface of Java.
* It stores the data in (Key, Value) pairs, and you can access them by an index of another type (e.g. an Integer).
* One object is used as a key (index) to another object (value).
* If you try to insert the duplicate key, it will replace the element of the corresponding key.
*/

public class HashmapEx {
    // method return HashMap object with data pairs
    static HashMap logindata() {
        HashMap hm = new HashMap();
        hm.put("x", "mercury@mercury");
        hm.put("y", "mercury1@mercury1");
        hm.put("z", "mercury2@mercury2");

        return hm;
    }

    @Test
    public void useHashLogin(){

        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();

        driver.get("http://demo.guru99.com/test/newtours/");

        // Login as X
        String credentials = (String) logindata().get("x"); // Retriving value 'x' from HashMap

        String uarr[] = credentials.split("@"); // separating value of 'x' into 2 parts using delimiter '@'

        driver.findElement(By.name("userName")).sendKeys(uarr[0]); // Passing value 1 i.e username from array
        driver.findElement(By.name("password")).sendKeys(uarr[1]); // Passing value 2 i.e password from array
        driver.findElement(By.name("submit")).click();

        driver.quit();
    }
}