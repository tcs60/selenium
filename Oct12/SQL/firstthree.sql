/*How to get only first 3 record*/
SELECT contact_id, last_name, first_name
FROM contacts
WHERE website = 'TechOnTheNet.com'
ORDER BY contact_id
LIMIT 3;

/*How to get only first 3 record (in Microsoft SQLs)*/

SELECT TOP(3) contact_id, last_name, first_name
FROM contacts
WHERE website = 'TechOnTheNet.com'
ORDER BY contact_id;
