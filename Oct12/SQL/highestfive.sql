/*How to find highest five column record (in SQL)*/
SELECT contact_id, last_name, first_name
FROM contacts
WHERE website = 'TechOnTheNet.com'
ORDER BY contact_id DESC
LIMIT 5;

/*How to find highest five column record (in Microsoft SQLs)*/

SELECT TOP(5) contact_id, last_name, first_name
FROM contacts
WHERE website = 'TechOnTheNet.com'
ORDER BY contact_id;
