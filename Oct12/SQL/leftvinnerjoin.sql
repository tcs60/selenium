/*Left join vs inner join*/
SELECT * 
FROM Empleados E
JOIN Departamentos D /*INNER is implicit: Selects only matching items between the 2 tables*/
ON E.DepartamentoId = D.Id;

SELECT
  E.Nombre as 'Empleado',
  D.Nombre as 'Departamento'
FROM Empleados E
LEFT JOIN Departamentos D /*LEFT Selects all items from the first table and matches with the second table, if theres no match on the second table, it throws NULL with the correct item from the first table*/
ON E.DepartamentoId = D.Id
