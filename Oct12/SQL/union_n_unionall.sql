/*Union and union all*/
SELECT City FROM Customers
UNION /*Displays all distinct values from each SELECT query, so if there are equal values in both SELECTs, they're only displayed once*/
SELECT City FROM Suppliers
ORDER BY City;

SELECT City FROM Customers
UNION ALL /*Displays all values from each SELECT query, so if there are equal values in both SELECTs, they're also displayed*/
SELECT City FROM Suppliers
ORDER BY City;
