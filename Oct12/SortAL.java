import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

/*
* In Java, ArrayList is a class of Collections framework that is defined in the java.util package.
* It inherits the AbstractList class. It dynamically stores the elements.
*
* An ArrayList can be sorted by using the sort() method of the Collections class in Java.
*/

public class SortAL {
    @Test
    public void sortAl(){
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(566);
        list.add(230);
        list.add(123);
        list.add(110);
        list.add(689);
        list.add(12);
        list.add(95);
        //printing ArrayList before sorting
        System.out.println("ArrayList Before Sorting:");
        for(int marks: list){
            System.out.println(marks);
        }
        //sorting ArrayList in descending order
        Collections.sort(list, Collections.reverseOrder());
        //printing ArrayList after sorting
        System.out.println("ArrayList After Sorting:");
        for(int marks: list){
            System.out.println(marks);
        }
    }
}
