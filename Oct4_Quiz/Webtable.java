import com.excel.lib.util.Xls_Reader;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

public class Webtable {
    //"D:/Julio/Documents/Chamba/tata/training/quiz/quiz.xlsx"
    //Sheet1
    //https://cosmocode.io/automation-practice-webtable/
    ////table[@id = "countries"]/tbody/tr/td/h3
    ////table[@id = "countries"]/tbody/tr
    @Test
    public void printTable(){
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://cosmocode.io/automation-practice-webtable/";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        //Fetch the Excel file
        Xls_Reader reader = new Xls_Reader("D:/Julio/Documents/Chamba/tata/training/quiz/quiz.xlsx"); //XLSX File path
        String sheet = "Sheet1";

        List<WebElement> trows = driver.findElements(By.xpath("//table[@id = \"countries\"]/tbody/tr"));
        List<WebElement> tcols = driver.findElements(By.xpath("//table[@id = \"countries\"]/tbody/tr/td/h3"));

        int rows = trows.size(), cols = tcols.size();

        for (int i = 1; i <= rows; i++){
            for (int j = 0; j < cols; j++){
                if (i == 1){
                    WebElement tdata = driver.findElement(By.xpath("//table[@id = \"countries\"]/tbody/tr[" + i + "]/td[" + (j+1) + "]"));
                    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", tdata);
                    String input = tdata.getText();
                    reader.addColumn(sheet,input);
                }else if(j<4){
                    WebElement tdata = driver.findElement(By.xpath("//table[@id = \"countries\"]/tbody/tr[" + i + "]/td[" + (j+2) + "]"));
                    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", tdata);
                    String input = tdata.getText(),
                            header = reader.getCellData(sheet, j+1, 1);
                    reader.setCellData(sheet, header, i, input);
                }
            }
        }

        driver.quit();
    }
}
