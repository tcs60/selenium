/*
* Exceptions are events due to which java program ends abruptly without giving expected output.
* Java provides a framework where a user can handle exceptions.
* Exceptions need to be handled because they break the normal flow of execution of a program.
*/
import org.junit.Test;
import org.openqa.selenium.*;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class Exceptions {
    @Test (expected = NoSuchElementException.class)
    public void noSuchElement(){ //The exception occurs when WebDriver is unable to find and locate elements.
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://cosmocode.io/automation-practice-webtable/";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        driver.findElement(By.id("counties")).click();
        driver.quit();
    }
    @Test (expected = NoSuchWindowException.class)
    public void noSuchWindow(){ //This is thrown when WebDriver tries to switch to an invalid window.
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://cosmocode.io/automation-practice-webtable/";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        String handle_1 = driver.getWindowHandle();
        driver.switchTo().window("handle1_");
        driver.quit();
    }
    @Test (expected = NoSuchFrameException.class)
    public void noSuchFrame(){ //Thrown when WebDriver is trying to switch to an invalid frame.
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://cosmocode.io/automation-practice-webtable/";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        driver.switchTo().frame("main-frame");
        driver.quit();
    }
    @Test (expected = NoAlertPresentException.class)
    public void noAlertPresent(){ //Thrown when WebDriver tries to switch to an alert, which is not available.
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://cosmocode.io/automation-practice-webtable/";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        driver.switchTo().alert().accept();
        driver.quit();
    }
    @Test (expected = InvalidSelectorException.class)
    public void invalidSelector(){ //Is thrown when a selector is incorrect or syntactically invalid.
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://cosmocode.io/automation-practice-webtable/";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        driver.findElement(By.xpath(".//*table/tbody/tr[198]")).click();
        driver.quit();
    }
    @Test (expected = ElementNotInteractableException.class)
    public void elementNotInteractable(){ //Thrown when WebDriver tries to perform an action on an invisible web element, which cannot be interacted with.
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://cosmocode.io/automation-practice-webtable/";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        driver.findElement(By.linkText("About Us")).click();

        driver.quit();
    }
    @Test
    public void elementNotSelectable(){ //ElementNotSelectableException indicates that the web element is present in the web page but cannot be selected.
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://html.com/attributes/button-disabled/";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/div[3]/article/div/div[2]/button")).click();

        driver.quit();
    }
    @Test
    public void timeout(){  //if the components don’t load even after the wait, the exception TimeOut will be thrown.
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://www.google.com/";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS) ;

        driver.quit();
    }
    @Test (expected = NoSuchSessionException.class)
    public void noSuchSession(){ //This exception is thrown when a method is called after quitting the browser by WebDriver.quit()
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://cosmocode.io/automation-practice-webtable/";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        driver.quit();

        Select dropdown = new Select(driver.findElement(By.id("swift")));
    }
    @Test (expected = StaleElementReferenceException.class)
    public void staleElement(){ //This exception says that a previously loaded web element is no longer present in the web page.
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://formy-project.herokuapp.com";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        WebElement opt1 = driver.findElement(By.xpath("/html/body/div/div/li[1]/a"));
        WebElement opt2 = driver.findElement(By.xpath("/html/body/div/div/li[2]/a"));

        opt1.click();
        WebElement autocomp = driver.findElement(By.id("autocomplete"));

        opt2.click();

        driver.quit();
    }
}
