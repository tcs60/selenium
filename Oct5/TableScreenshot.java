import org.junit.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class TableScreenshot {
    @Test
    public void takeSS() throws IOException {
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://cosmocode.io/automation-practice-webtable/";

        //Start webbrowser
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        //Locate table
        WebElement table = driver.findElement(By.id("countries"));

        //Take screenshot of the whole page, scrolling down with pauses
        Screenshot s = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);

        //Create a File object to save the screenshot
        File screenshot = new File("D:\\Julio\\Documents\\Chamba\\tata\\training\\Oct5\\imgs\\full.png");

        //Save the full screenshot in the path of the File object
        ImageIO.write(s.getImage(), "PNG", screenshot);

        //Buffer the full screenshot so we can crop it
        BufferedImage fullScreen = ImageIO.read(screenshot);

        //The buffer is like a map of the actual webpage, we locate the table with this
        Point location = table.getLocation();

        //We save the size of the actual table
        int width = table.getSize().getWidth();
        int height = table.getSize().getHeight();

        //We crop the image
        BufferedImage tableSS = fullScreen.getSubimage(location.getX(), location.getY(), width, height);

        //We save the cropped image
        ImageIO.write(tableSS, "png", new File("D:\\Julio\\Documents\\Chamba\\tata\\training\\Oct5\\imgs\\table.png"));

        driver.quit();
    }
}
