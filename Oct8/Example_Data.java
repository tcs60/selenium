import com.excel.lib.util.Xls_Reader;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class Example_Data {
    @Test
    public void addNumbers(){
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        //Fetch the Excel file
        Xls_Reader reader = new Xls_Reader("D:\\Julio\\Documents\\Chamba\\tata\\training\\Oct8\\SumValues.xlsx"); //XLSX File path
        String sheet = "Sheet1";

        int rows = reader.getRowCount(sheet), cols = reader.getColumnCount(sheet);

        driver.manage().timeouts().implicitlyWait(1500, TimeUnit.MILLISECONDS);
        driver.findElement(By.id("at-cv-lightbox-close")).click();

        WebElement fstNum = driver.findElement(By.id("sum1"));
        WebElement sndNum = driver.findElement(By.id("sum2"));
        WebElement result = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div[2]/form/button"));

        for (int i = 1; i <= rows; i++) {
            fstNum.sendKeys(reader.getCellData(sheet, i, i-1));
            sndNum.sendKeys(reader.getCellData(sheet, i, i));
            result.click();

            driver.manage().timeouts().implicitlyWait(1500, TimeUnit.MILLISECONDS);
        }

        driver.quit();
    }
}
