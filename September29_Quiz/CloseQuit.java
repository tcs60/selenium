import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/*
* Driver.close() is used to just close the current window but not finnish the driver usage.
* Driver.quit() is used to exit the browser, end the session, tabs, pop-ups etc.
* When dealing with multiple tabs or windows, it's better to use Driver.close()
*/

public class CloseQuit {
    public static void main(String[] args) throws InterruptedException {
        String geckoPath = "D:/Julio/Documents/Chamba/tata/training/geckodriver-v0.30.0-win64/geckodriver.exe";
        System.setProperty("webdriver.gecko.driver", geckoPath);
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.google.com");
        Thread.sleep(2000);//Open first tab

        WebElement body = driver.findElement(By.tagName("body"));
        body.sendKeys(Keys.chord(Keys.CONTROL, "t"));
        driver.get("https://www.google.com");//Open second tab


        body.sendKeys(Keys.chord(Keys.CONTROL, "t"));
        driver.get("https://www.google.com");//Open third tab
        Thread.sleep(2000);

        driver.close();//Close third tab
        Thread.sleep(2000);
        driver.quit();//Close Browser
    }
}
