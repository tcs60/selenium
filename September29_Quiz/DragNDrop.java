/*
 * Drag and Drop is a useful functionality in web to rearrange elements or manipulate media nowadays.
 *
 * To implement this, it´s necessary to instance the Actions class, and we need a source item and destination item.
 *
 * Basically, the movement of drag and drop is a vector of the item to be moved
 *
 */

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class DragNDrop {
    public static void main(String[] args) throws InterruptedException {
        String geckoPath = "D:/Julio/Documents/Chamba/tata/training/geckodriver-v0.30.0-win64/geckodriver.exe";
        System.setProperty("webdriver.gecko.driver", geckoPath);
        WebDriver driver = new FirefoxDriver();
        driver.get("https://formy-project.herokuapp.com/dragdrop");

        WebElement source = driver.findElement(By.id("image"));
        WebElement destination = driver.findElement(By.id("box"));

        Thread.sleep(2000);

        Actions actions = new Actions(driver);
        actions.dragAndDrop(source,destination).build().perform();
        //After we set the source and destination, we create the movement simulation, and then we can run that movement.

        Thread.sleep(2000);

        driver.quit();
    }
}
