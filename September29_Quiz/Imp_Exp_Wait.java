import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/*
* Implicit wait commands the test to hold for the specific amount of time regarding the operation, but it may take mor time than necessary
* Explicit wait is more intelligent, waits just the right time until the specified action is available
*
* */

public class Imp_Exp_Wait {
    public static void main(String[] args) {
        String geckoPath = "D:/Julio/Documents/Chamba/tata/training/geckodriver-v0.30.0-win64/geckodriver.exe";
        System.setProperty("webdriver.gecko.driver", geckoPath);
        WebDriver driver = new FirefoxDriver();
        driver.get("https://formy-project.herokuapp.com");

        WebElement opt1 = driver.findElement(By.xpath("/html/body/div/div/li[1]/a"));
        WebElement opt2 = driver.findElement(By.xpath("/html/body/div/div/li[2]/a"));
        WebElement opt3 = driver.findElement(By.xpath("/html/body/div/div/li[3]/a"));

        opt1.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); //Implicit wait
        WebElement autocomp = driver.findElement(By.id("autocomplete"));


        autocomp.sendKeys("76079 via sovana inidian wells");

        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement suggestions = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("pac-item")));//Explicit Wait

        suggestions.click();
        driver.quit();


    }
}
