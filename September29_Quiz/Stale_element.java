/*
* Stale means old, decayed, no longer fresh. The Stale Element exception happens when during our test
* we have elements that in some point we used but sometime after it cannot be accessed again.
*
* It commonly happens when it no longer becomes available, or we change pages or sections
* during the test.
*
*/

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Stale_element {
    public static void main(String[] args) throws InterruptedException {
        String geckoPath = "D:/Julio/Documents/Chamba/tata/training/geckodriver-v0.30.0-win64/geckodriver.exe";
        System.setProperty("webdriver.gecko.driver", geckoPath);
        WebDriver driver = new FirefoxDriver();
        driver.get("https://formy-project.herokuapp.com");

        WebElement opt1 = driver.findElement(By.xpath("/html/body/div/div/li[1]/a"));
        WebElement opt2 = driver.findElement(By.xpath("/html/body/div/div/li[2]/a"));
        WebElement opt3 = driver.findElement(By.xpath("/html/body/div/div/li[3]/a"));

        opt1.click();
        WebElement autocomp = driver.findElement(By.id("autocomplete"));
        //Thread.sleep(2000); //This solves the stale element issue
        autocomp.sendKeys("76079 via sovana inidian wells");
        Thread.sleep(2000);
        WebElement suggestions = driver.findElement(By.className("pac-item"));
        suggestions.click(); // In this very example, it detects a stale element because the suggestions cannot load in time due to the ver fast run of the program
        driver.quit();
    }
}
