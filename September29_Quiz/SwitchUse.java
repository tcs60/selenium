import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

/*
* Driver.switchTo() can be used for windows, frames and tabs. It switches between those elements
*
*/

public class SwitchUse {
    public static void main(String[] args) {
        String geckoPath = "D:/Julio/Documents/Chamba/tata/training/geckodriver-v0.30.0-win64/geckodriver.exe";
        System.setProperty("webdriver.gecko.driver", geckoPath);
        WebDriver driver = new FirefoxDriver();

        driver.get("https://demoqa.com/frames");//Start page with frames

        driver.switchTo().frame(driver.findElement(By.id("frame1")));//change to a frame
        driver.switchTo().defaultContent();//go back to main "frame"

        WebElement wind_ex = driver.findElement(By.id("item-0"));//switch to page that loads new tabs and windows
        wind_ex.click();

        WebElement newTabButton = driver.findElement(By.id("tabButton"));//open a new tab
        newTabButton.click();

        String originalHandle = driver.getWindowHandle();//save original tab

        for(String handle1: driver.getWindowHandles()) {
            driver.switchTo().window(handle1);//switch to new tab
        }

        driver.switchTo().window(originalHandle);//return to original tab

        WebElement newWinButton = driver.findElement(By.id("windowButton"));//open a new window
        newTabButton.click();

        driver.switchTo().window(originalHandle);//return to original window

        driver.quit();
    }
}
