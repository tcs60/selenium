import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PassCheck {
    WebDriver driver;

    By loginpage = By.className("login");
    By passwd = By.id("passwd");
    By outside = By.xpath("/html/body/div/div[2]/div/div[3]/div/div/div[2]/form/div/div[2]");

    public PassCheck(WebDriver driver){
        this.driver=driver;
    }

    public void validatePwd(String notvalid, String valid) throws InterruptedException {
        driver.findElement(loginpage).click();
        Thread.sleep(7000);
        driver.findElement(passwd).sendKeys(notvalid);
        Thread.sleep(3000);
        driver.findElement(outside).click();
        driver.findElement(passwd).sendKeys(valid);
        Thread.sleep(3000);
    }
}
