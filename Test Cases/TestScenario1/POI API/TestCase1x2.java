import com.excel.lib.util.Xls_Reader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestCase1x2 {
    public static void main(String[] args) throws InterruptedException {
        //Normal web driver initialization of selenium
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "http://automationpractice.com/index.php";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        //Fetch the Excel data
        Xls_Reader reader = new Xls_Reader("D:\\Julio\\Documents\\Chamba\\tata\\training\\Test Cases\\TestData.xlsx"); //XLSX File path
        String sheet = "TestData";

        //Variables where we save the data fetched from the xlsx file
        String not_valid = reader.getCellData(sheet,1,2),
                valid = reader.getCellData(sheet,1,3);

        Thread.sleep(10000);

        PassCheck pwd = new PassCheck(driver);

        pwd.validatePwd(not_valid,valid);

        driver.quit();
    }
}
