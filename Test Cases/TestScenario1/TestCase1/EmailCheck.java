import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EmailCheck {
    WebDriver driver;

    By loginpage = By.className("login");
    By email = By.id("email");
    By outside = By.xpath("/html/body/div/div[2]/div/div[3]/div/div/div[2]/form/div/div[1]");

    public EmailCheck(WebDriver driver){
        this.driver=driver;
    }

    public void validateEmail(String notvalid, String valid) throws InterruptedException {
        driver.findElement(loginpage).click();
        Thread.sleep(7000);
        driver.findElement(email).sendKeys(notvalid);
        Thread.sleep(3000);
        driver.findElement(outside).click();
        driver.findElement(email).sendKeys(valid);
        Thread.sleep(3000);
    }
}
