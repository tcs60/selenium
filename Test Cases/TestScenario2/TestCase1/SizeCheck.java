import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SizeCheck {
    WebDriver driver;

    By home = By.linkText("http://automationpractice.com/");
    By item = By.cssSelector("#homefeatured > li:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(3) > span:nth-child(1)");
    By sizebox = By.id("group_1");
    By sizeL = By.xpath("/html/body/div/div/div[3]/form/div/div[2]/div/fieldset[1]/div/div/select/option[3]");
    By checkout = By.tagName("Submit");

    public SizeCheck(WebDriver driver){
        this.driver=driver;
    }

    public void selectSize() throws InterruptedException {
        driver.findElement(home).click();
        Thread.sleep(12000);
        driver.findElement(item).click();
        Thread.sleep(8000);
        driver.findElement(sizebox).click();
        Thread.sleep(3000);
        driver.findElement(sizebox).findElement(sizeL).click();
        Thread.sleep(3000);
        driver.findElement(checkout).click();
        Thread.sleep(6000);
    }
}
