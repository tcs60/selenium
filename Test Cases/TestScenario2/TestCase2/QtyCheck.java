import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class QtyCheck {
    WebDriver driver;

    By home = By.linkText("http://automationpractice.com/");
    By item = By.cssSelector("#homefeatured > li:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(3) > span:nth-child(1)");
    By addbutton = By.className("icon-plus");
    By checkout = By.tagName("Submit");

    public QtyCheck(WebDriver driver){
        this.driver=driver;
    }

    public void selectSize() throws InterruptedException {
        driver.findElement(home).click();
        Thread.sleep(12000);
        driver.findElement(item).click();
        Thread.sleep(8000);
        driver.findElement(addbutton).click();
        Thread.sleep(500);
        driver.findElement(addbutton).click();
        Thread.sleep(500);
        driver.findElement(checkout).click();
        Thread.sleep(6000);
    }
}
