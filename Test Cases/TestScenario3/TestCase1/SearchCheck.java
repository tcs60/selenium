import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchCheck {
    WebDriver driver;

    By home = By.id("header_logo");
    By searchbox = By.id("search_query_top");
    By searchbtn = By.name("submit_search");

    public SearchCheck(WebDriver driver){
        this.driver=driver;
    }

    public void validSearch() throws InterruptedException {
        driver.findElement(home).click();
        Thread.sleep(2000);
        driver.findElement(searchbox).sendKeys("dress");
        Thread.sleep(4000);
        driver.findElement(searchbtn).click();
        Thread.sleep(2000);
    }
}
