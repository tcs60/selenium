import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestCase3x1 {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.gecko.driver", "D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.30.0-win64\\geckodriver.exe");
        String webpage = "http://automationpractice.com/index.php";

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(webpage);

        Thread.sleep(3000);

        SearchCheck sch = new SearchCheck(driver);

        sch.validSearch();

        driver.quit();
    }
}

