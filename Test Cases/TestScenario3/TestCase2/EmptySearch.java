import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EmptySearch {
    WebDriver driver;

    By home = By.linkText("http://automationpractice.com/");
    By searchbtn = By.name("submit_search");

    public EmptySearch(WebDriver driver){
        this.driver=driver;
    }

    public void invalidSearch() throws InterruptedException {
        driver.findElement(home).click();
        Thread.sleep(12000);
        driver.findElement(searchbtn).click();
        Thread.sleep(5000);
    }
}
