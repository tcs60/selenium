import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;

public class Test1 {
    public static void main(String[] args) {
        System.setProperty("webdriver.firefox.driver","D:/Julio/Documents/Chamba/tata/training/geckodriver-v0.29.1-win64/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://formy-project.herokuapp.com");
        WebElement opt1 = driver.findElement(By.xpath("/html/body/div/div/li[1]/a"));
        WebElement opt2 = driver.findElement(By.xpath("/html/body/div/div/li[2]/a"));
        WebElement opt3 = driver.findElement(By.xpath("/html/body/div/div/li[3]/a"));

        opt1.click();
        WebElement autocomp = driver.findElement(By.id("autocomplete"));
        autocomp.sendKeys("76079 Via Sovana Inidian Wells");
        WebElement suggestions = driver.findElement(By.className("pac-item"));
        suggestions.click();
        driver.quit();
    }
}
