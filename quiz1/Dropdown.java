package quiz2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Dropdown {
    public static void main(String[] args) {
        System.setProperty("webdriver.firefox.driver","D:/Julio/Documents/Chamba/tata/training/geckodriver-v0.29.1-win64/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://formy-project.herokuapp.com/dropdown");
        WebElement dropd = driver.findElement(By.id("dropdownMenuButton"));// We need to find first the dropdown selector
        dropd.click();
        WebElement drop_opt = driver.findElement(By.id("autocomplete"));//We need to search the elements of the dropdown
        drop_opt.click();
    }
}
