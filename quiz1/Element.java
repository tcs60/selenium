package quiz2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

public class Element {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver","\\D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.29.1-win64\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://formy-project.herokuapp.com");
        List<WebElement> opt1 = driver.findElements(By.className("btn")); //Find elements MUST be declared as a list because it is what it throws
        WebElement opt2 = driver.findElement(By.xpath("/html/body/div/div/li[2]/a")); //It is just one element
    }
}
