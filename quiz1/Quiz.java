package quiz2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Quiz {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver","\\D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.29.1-win64\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://formy-project.herokuapp.com");
        WebElement opt1 = driver.findElement(By.xpath("/html/body/div/div/li[1]/a"));
        WebElement opt2 = driver.findElement(By.className("btn btn-lg"));
        WebElement opt3 = driver.findElement(By.cssSelector(".jumbotron-fluid > li:nth-child(6) > a:nth-child(1)"));
        //Other types of elements not present in the page used for the example
        WebElement opt4 = driver.findElement(By.id(""));
        WebElement opt5 = driver.findElement(By.name(""));
        WebElement opt6= driver.findElement(By.linkText(""));
        WebElement opt7 = driver.findElement(By.partialLinkText(""));
        WebElement opt8 = driver.findElement(By.tagName(""));
        driver.quit();
    }
}
