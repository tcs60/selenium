package quiz2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Xpath {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver","\\D:\\Julio\\Documents\\Chamba\\tata\\training\\geckodriver-v0.29.1-win64\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://formy-project.herokuapp.com");
        WebElement opt1 = driver.findElement(By.xpath("/html/body/div/div/li[1]/a")); //ABSOLUTE XPATH
        WebElement opt2 = driver.findElement(By.xpath("//*/li[2]/a")); //RELATIVE XPATH
    }
}
